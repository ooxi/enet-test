#include <enet/enet.h>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <iostream>
#include <unistd.h>



/**
 */
int main(int argc, char** argv) {
	if (enet_initialize() != 0) {
		std::cerr << "An error occurred while initializing ENet." << std::endl;
		return EXIT_FAILURE;
	}
	atexit(enet_deinitialize);
	
	
	ENetAddress address;

	/* Bind the server to the default localhost.     */
	/* A specific host address can be specified by   */
	/* enet_address_set_host (& address, "x.x.x.x"); */
	address.host = ENET_HOST_ANY;
	
	/* Bind the server to port 1234. */
	address.port = 1234;
	
	
	ENetHost* server = enet_host_create(
		&address /* the address to bind the server host to */, 
		32      /* allow up to 32 clients and/or outgoing connections */,
		2      /* allow up to 2 channels to be used, 0 and 1 */,
		0      /* assume any amount of incoming bandwidth */,
		0      /* assume any amount of outgoing bandwidth */
	);
	if (!server) {
		std::cerr << "An error occurred while trying to create an ENet server host." << std::endl;
		return EXIT_FAILURE;
	}

	
	bool run = true;
	ENetEvent event;
	
	/* Wait up to 1000 milliseconds for an event. */
	while (run) {
		int rc = enet_host_service(server, &event, 1000);
		
		if (rc < 0) {
			std::cerr << "enet_host_service error" << std::endl;
			run = false;
			continue;
		} else if (rc == 0) {
			std::cout << "No events" << std::endl;
			sleep(1);
			continue;
		}
		std::cout << "Received event type #" << event.type << std::endl;
		
		
		switch (event.type) {
			case ENET_EVENT_TYPE_CONNECT:
				printf("A new client connected from %x:%u.\n", 
					event.peer -> address.host,
					event.peer -> address.port
				);
				
				/* Store any relevant client information here. */
//				event.peer -> data = strdup("Client information");
				break;
			
			case ENET_EVENT_TYPE_RECEIVE:
				printf("A packet of length %u containing %s was received from %s on channel %u.\n",
					event.packet -> dataLength,
					event.packet -> data,
					event.peer -> data,
					event.channelID
				);
				
				/* Clean up the packet now that we're done using it. */
				enet_packet_destroy (event.packet);
				break;
       
			case ENET_EVENT_TYPE_DISCONNECT:
				printf("%s disconnected.\n", event.peer -> data);
				
				/* Reset the peer's client information. */
				free(event.peer->data);
				event.peer -> data = NULL;
				run = false;
				break;
		}
	}
	
	
	enet_host_destroy(server);



	std::cout << "Hello Server" << std::endl;
	return 0;
}
