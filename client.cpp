#include <enet/enet.h>
#include <cstdlib>
#include <cstring>
#include <iostream>



/**
 */
int main(int argc, char** argv) {
	if (enet_initialize() != 0) {
		std::cerr << "An error occurred while initializing ENet." << std::endl;
		return EXIT_FAILURE;
	}
	atexit(enet_deinitialize);
	
	
	
	
	
	ENetHost* client = enet_host_create(
		NULL /* create a client host */,
		1 /* only allow 1 outgoing connection */,
		2 /* allow up 2 channels to be used, 0 and 1 */,
		57600 / 8 /* 56K modem with 56 Kbps downstream bandwidth */,
		14400 / 8 /* 56K modem with 14 Kbps upstream bandwidth */
	);
	if (!client) {
		std::cerr << "An error occurred while trying to create an ENet client host." << std::endl;
		return EXIT_FAILURE;
	}

	
	ENetAddress address;
	ENetEvent event;
	ENetPeer *peer;
	
	/* Connect to some.server.net:1234. */
	enet_address_set_host (&address, "localhost");
	address.port = 1234;
	
	/* Initiate the connection, allocating the two channels 0 and 1. */
	peer = enet_host_connect (client, &address, 2, 0);
	
	if (!peer) {
		std::cerr << "No available peers for initiating an ENet connection." << std::endl;
		return EXIT_FAILURE;
	}
	
	/* Wait up to 5 seconds for the connection attempt to succeed. */
	if (enet_host_service (client, & event, 5000) > 0 && event.type == ENET_EVENT_TYPE_CONNECT) {
		std::cout << "Connection to localhost:1234 succeeded." << std::endl;

		
		
		
		
		/* Create a reliable packet of size 7 containing "packet\0" */
		ENetPacket * packet = enet_packet_create ("packet",  strlen ("packet") + 1, ENET_PACKET_FLAG_RELIABLE);
		
		/* Extend the packet so and append the string "foo", so it now */
		/* contains "packetfoo\0"                                      */
		enet_packet_resize (packet, strlen ("packetfoo") + 1);
		strcpy ((char*)& packet -> data [strlen ("packet")], "foo");
		
		/* Send the packet to the peer over channel id 0. */
		/* One could also broadcast the packet by         */
		/* enet_host_broadcast (host, 0, packet);         */
		enet_peer_send (peer, 0, packet);

		/* One could just use enet_host_service() instead. */
		enet_host_flush (client);

		
		
		
		
		ENetEvent event;
		enet_peer_disconnect (peer, 0);
		
		/* Allow up to 3 seconds for the disconnect to succeed
		 * and drop any packets received packets.
		 */
		bool done = false;
		
		while (!done && enet_host_service (client, & event, 3000) > 0) {
			switch (event.type) {
				case ENET_EVENT_TYPE_RECEIVE:
					enet_packet_destroy (event.packet);
					break;
					
				case ENET_EVENT_TYPE_DISCONNECT:
					std::cout << "Disconnection succeeded." << std::endl;
					done = true;
					break;
			}
		}
		
		/* We've arrived here, so the disconnect attempt didn't */
		/* succeed yet.  Force the connection down.             */
		if (!done) {
			enet_peer_reset (peer);
		}

	} else {
		/* Either the 5 seconds are up or a disconnect event was */
		/* received. Reset the peer in the event the 5 seconds   */
		/* had run out without any significant event.            */
		enet_peer_reset (peer);
		std::cout << "Connection to localhost:1234 failed." << std::endl;
	}
	
	enet_host_destroy(client);

	
	std::cout << "Hello Client" << std::endl;
	return 0;
}
